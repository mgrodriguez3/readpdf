ReadPdf is a little programme to check in the directory /home/userName/analisePDFs 
PDF files if some words appears or they are repeated. These words are: "AEROSOL", "NO2", "AUSTRALIA", "MALASYA","BRITAIN", "RAINFALL", "WIND", and "DUST". These words are used to classify files in three categories: Accepted, corrupted, and discarded. These words are used to classify PDFs according to the following criterias:

- Documents containing aerosol, NO2, Australia, Britain or Malaysia are discarded;
- Documents containing rainfall but not wind are discarded;
- Documents that contain dust but do not contain wind are discarded;
- Documents with a format distinct to PDF are classified as corrupted. Malformed PDFs are also classified as corrupted;
- The rest of the documents and that, therefore, have passed the previous filters successfully, are classified as accepted.


The following directories are created with the aim to include the results of the analysis on in: 

    - /home/userName/analisePDFs. Paste here all the PDFs to analyse before executing ReadPdf.
    - /home/userName/analisePDFsDiscarded. Discarded files.
    - /home/userName/analisePDFsCorrupted. Files to analyse manually because the PDF is malformed.
    - /home/userName/analisePDFAccepted. Files that meet the search criteria.

You can change the words to check in the string variables FILTERS and EXCEPTIONS. 


To execute it: 

	1) From a terminal

		- access the unzipped folder and the "target" directory
		- run the file "ReadPdf-1.0-jar-with-dependencies.jar" with this command: 
			
			--> "java -jar ReadPdf-1.0-jar-with-dependencies.jar"

	2) Through a development environment

		- import the project from an IDE;
		- run it.
