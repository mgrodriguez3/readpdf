/**
 * @author: Michael García Rodríguez
 * @date: 09/06/2020
 * @version: 1.2
 *
 * This little software filter in the directory /home/userName/analizarPDFs
 * several words in PDF file and move not valid files to analizarPDFs Results.
 *
 *
 *
 * This programme classify all readed files in 3 cathegories: - Accepted: files
 * that not contains one or more words from FILTERS list and exceptions files
 * explained below ; - Discarded: files that contains one or more words from
 * FILTERS list and exceptions files explained below; - Corrupted: files that
 * need a manually revision because PDF id malformed.
 *
 * Exceptions words to not discard document if contains: rainfall + wind =
 * accepted; rainfall + !wind = discarded; dust + wind = accepted; dust + !wind
 * = discarded.
 *
 */
package es.uvigo.esei.ephyslab.readpdf;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.io.FilenameUtils;


public class ReadPdf {

    /**
     * The extension file to check
     */
    private static final String EXTENSION = "pdf";

    /**
     * The path to analyse.
     */
    private static final String DISCARDED = System.getProperty("user.home") + "/analisePDFsDiscarded";

    /**
     * The path with corrupted files.
     */
    private static final String CORRUPTED = System.getProperty("user.home") + "/analisePDFsCorrupted";

    /**
     * The path with all files accepted.
     */
    private static final String ACCEPTED = System.getProperty("user.home") + "/analisePDFAccepted";

    /**
     * Words to discart documents if contains one or more on files readed.
     */
    private static final String[] FILTERS = {"AEROSOL", "NO2", "AUSTRALIA", "BRITAIN", "MALAYSIA"};

    /**
     * Words to check conditions for accepted files.
     */
    private static final String[] EXCEPTIONS = {"RAINFALL", "WIND", "DUST"};
    
    
   /**
    * Regular Expression to find words to match exactly with them and not with
    * words formed by them.
    */ 
    private static String REGEX_FIND_WORD="(?i).*?\\b%s\\b.*?";
    
    
    public static String REGEX_FIND_WORD2 = "\b%s\b";
    
    /**
     * the path of the destination of the file.
     *
     */
    public static final String PATH = System.getProperty("user.home") + "/analisePDFs";

    public static void main(String[] args) throws IOException {

        /**
         * List of files in folder and subfolders.
         */
        List<File> filesInFolder;

        File dis = new File(ReadPdf.DISCARDED);
        File dest = new File(ReadPdf.ACCEPTED);
        File corr = new File(ReadPdf.CORRUPTED);

        filesInFolder = Files.walk(Paths.get(ReadPdf.PATH))
                .filter(Files::isRegularFile)
                .map(java.nio.file.Path::toFile)
                .collect(Collectors.toList());

        /**
         * Check if the destination directory exists.
         */
        if (!dis.exists()) {
            new File(ReadPdf.DISCARDED).mkdirs();
        }

        /**
         * Check if the accepted directory exists.
         */
        if (!dest.exists()) {
            new File(ReadPdf.ACCEPTED).mkdirs();
        }

        /**
         * Check if the corrupted directory exists.
         */
        if (!corr.exists()) {
            new File(ReadPdf.CORRUPTED).mkdirs();
        }

        /**
         * For each file of the directory and subdirectory.
         */
        filesInFolder.forEach(new Consumer<File>() {
            @Override
            public void accept(File file) {
                try {
                    System.out.println("File Name:" + file.getName());
                    
                    if (FilenameUtils.getExtension(file.getName()).equals(ReadPdf.EXTENSION)) {
                        
                        try (PDDocument document = PDDocument.load(file)) {
                            
                            document.getClass();
                            
                            /**
                             * Check the encription file.
                             */
                            if (!document.isEncrypted()) {
                                
                                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                                stripper.setSortByPosition(true);
                                
                                PDFTextStripper tStripper = new PDFTextStripper();
                                
                                String pdfFileInText = tStripper.getText(document);
                                
                                /**
                                 * split by whitespace
                                 */
                                String lines[] = pdfFileInText.split("\\r?\\n");
                                boolean accept = true;
                                boolean rainfall = false;
                                boolean wind = false;
                                boolean dust = false;
                                int referencesLine=0;
                                
                                for(int i = 0; i< lines.length; i++){
                                    String line = lines[i].toUpperCase();
                                    
                                    if(containsWord(line, "BIBLIOGRAPHY") || containsWord(line, "REFERENCES")){
                                        referencesLine = i;
                                    }
                                }
                                
                                /**
                                 * In case the PDF has no references section
                                 */
                                if(referencesLine == 0){
                                    referencesLine = lines.length;
                                }
                                
                                /**
                                 * The PDF read another time to check filters from
                                 * begining to reference or bibliography section
                                 */
                                for (int i = 0; i<referencesLine; i++) {
                                    String line = lines[i].toUpperCase();
                                    
                                    for (String filter : FILTERS) {
                                        if (containsWord(line, filter)) {
                                            accept = false;
                                            break;
                                            
                                        } else {
                                            
                                            if (containsWord(line, EXCEPTIONS[0])) {
                                                rainfall = true;
                                                
                                            }
                                            if (containsWord(line, EXCEPTIONS[1])) {
                                                wind = true;
                                            }
                                            if (containsWord(line, EXCEPTIONS[2])) {
                                                dust = true;
                                            }
                                            
                                            if ((rainfall && !wind) || (dust && !wind)) {
                                                accept = false;
                                                break;
                                            }
                                        }
                                    }
                                }
                                
                                if (accept) {
                                    System.out.println((char) 27 + "[34m" + "MATCH!");
                                    System.out.println((char) 27 + "[30m" + "-------------------------------------");
                                    file.renameTo(new File(ReadPdf.ACCEPTED + "/" + file.getName()));
                                } else {
                                    System.out.println((char) 27 + "[32m" + "ERROR!");
                                    file.renameTo(new File(ReadPdf.DISCARDED + "/" + file.getName()));
                                }
                                
                            }
                        } catch (Exception ex) {
                            System.out.println((char) 27 + "[31m" + "Corrupted PDF!");
                            file.renameTo(new File(ReadPdf.CORRUPTED + "/" + file.getName()));
                        }
                        
                    } else {
                        System.out.println((char) 27 + "[31m" + "Damaged PDF!");
                        file.renameTo(new File(ReadPdf.CORRUPTED + "/" + file.getName()));
                    }
                } catch (Exception | java.lang.NoClassDefFoundError e) {
                    System.out.println((char) 27 + "[31m" + "Malformed PDF!");
                    file.renameTo(new File(ReadPdf.CORRUPTED + "/" + file.getName()));
                }
            }
        });

    }

    private static boolean containsWord(String text, String word) {
        String regex = String.format(REGEX_FIND_WORD, Pattern.quote(word));
        return text.matches(regex);
    }
}
